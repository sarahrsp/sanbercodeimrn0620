
/*
  A. Balik String (10 poin)
    Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst.

    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 

  B. Palindrome (10 poin)
    Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
    Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
    Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
    Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 

  
    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 
  
  C. Bandingkan Angka (10 poin)
    Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 
    
*/

//Jawaban A
console.log('Soal 001 Part A (balikString)');
function balikString(string) {
    let reversed = '';
    for(let i = string.length - 1; i >= 0; i--) {
        reversed = reversed + string.charAt(i);
    }
    return reversed;
}

// TEST CASES balikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


//Jawaban B
console.log('Soal 001 Part B (palindrome)');
function palindrome(string) {
    let reversed = '';
    for(let i = string.length - 1; i >= 0; i--) {
        reversed = reversed + string.charAt(i);
        if (string == reversed) {
            tmp = false;
        } else {
            tmp = true;
        }
    }
    return reversed;
}

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false


//Jawaban C
const isIdentic     = (thing1, thing2) => JSON.stringify(thing1) === JSON.stringify(thing2);
const sortFn        = (a,b) => +a === +b ? 0 : +a > +b ? 1 : -1;
const sortDescFn    = (a,b) => +a === +b ? 0: +a < +b ? 1 : -1;
const orderFn       = (n1, n2) => [n1, n2].sort(sortFn);


console.log('Soal 001 Part C(bandingkan)');
function bandingkan(num1, num2) {
    if((num1, num2)<0) return -1;
    const [first, last] = orderFn(num1, num2);
    return Array.from({length: (num1>num2)}, (_, index) => index);
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 1818