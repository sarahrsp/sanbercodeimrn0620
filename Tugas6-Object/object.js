//Soal 1

function arrayToObject(arr) {
    if(!arr.length) {
        console.log("");
        return ""
    };
    const currentYear = (new Date()).getFullYear();
    const reduced = arr.reduce((a,b) => {
        const [firstName, lastName, gender, birthYear ] = b;
        const key = `${firstName} ${lastName}`;
        const age = !birthYear || birthYear > currentYear ? "Invalid birth year" : currentYear - birthYear;
        const obj = {
            firstName, lastName, gender, age
        }
        a[key] = obj;
        return a;
    },{});
// assuming the output is console log
Object.keys(reduced).forEach((key, index) => {
    console.log(index + 1 + '. ' + key + ' : ', reduced[key])
});

// assuming the output is an object
return reduced;
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


//Soal 2

function shoppingTime(memberId, money) {
    const products = [
        {name: 'Sepatu Stacattu', price: 1500000},
        {name: 'Baju Zoro', price: 500000},
        {name: 'Baju H&N', price: 250000},
        {name: 'Sweater Uniklooh', price: 175000},
        {name: 'Casing Handphone', price: 50000},
    ].sort((a,b) => a.price === b.price ? 0 : a.price > b.price ? -1 : 1);

    if(!memberId) return "Mohon maaf, toko X hanya berlaku untuk member saja";
    if(money < products[products.length -1].price) return "Mohon maaf, uang tidak cukup";

    return products.reduce((a,b) => {
        if(a.changeMoney >= b.price) {
            a.listPurchased.push(b.name);
            a.changeMoney -= b.price;
        }
        return a;
    }, {memberId, money, listPurchased: [], changeMoney: money })
    
}

   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


  //Soal 3

  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    const cost = 2000;
    return arrPenumpang.reduce((a,b) => {
        const [penumpang, naikDari, tujuan] = b;
        if(penumpang && naikDari && tujuan) {
            const bayar = (rute.indexOf(tujuan) - rute.indexOf(naikDari)) * cost;
            a.push({penumpang, naikDari, tujuan, bayar});
        }
        return a;
    }, [])
  }
 
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]