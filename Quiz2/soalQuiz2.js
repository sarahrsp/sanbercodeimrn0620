
/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

// Jawaban Soal 1 CLass Score
class Score {
    constructor({ subject, points, email}) {
        this._subject = subject;
        this._points = points.length ? [...points] : [points];
        this._email = email;
    }

    get subject() {
        return this._subject;
    }

    get email() {
        return this._email;
    }

    get points() {
        return this._points;
    }

    set points(p) {
        this._points = p.length ? [...p] : [p];
    }

    average() {
        return this.sum() / this._points.length;
    }

    sum() {
        return this._points.reduce((a,b) => a + b, 0);
    }
    
}


/*=========================================== 
    2. SOAL Create Score (10 Poin + 5 Poin ES6)
    ===========================================
    Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
    Function viewScores mengolah data email dan nilai skor pada parameter array 
    lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
    Contoh: 
  
    Input
     
    data : 
    [
      ["email", "quiz-1", "quiz-2", "quiz-3"],
      ["abduh@mail.com", 78, 89, 90],
      ["khairun@mail.com", 95, 85, 88]
    ]
    subject: "quiz-1"
  
    Output 
    [
      {email: "abduh@mail.com", subject: "quiz-1", points: 78},
      {email: "khairun@mail.com", subject: "quiz-1", points: 95},
    ]
  */


// Jawaban Soal 2 Create Score

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
  function viewScores(data, subject) {
    const [keys, ...datas] = data;
    const object =  datas.reduce((a,[email, ...scores]) => {
      let pointIndex = keys.indexOf(subject);

      // match test case subject format with the input subject format
      if(pointIndex === -1) pointIndex = keys.indexOf(subject.replace('-', ' - '));

      return [...a, { email, subject, points: scores[pointIndex - 1]}]
    }, []);

    console.log(object);
    return object;
  }
  
  // TEST CASE
  viewScores(data, "quiz-1")
  viewScores(data, "quiz-2")
  viewScores(data, "quiz-3")


/*==========================================
    3. SOAL Recap Score (15 Poin + 5 Poin ES6)
    ==========================================
      Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
      Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
      predikat kelulusan ditentukan dari aturan berikut:
      nilai > 70 "participant"
      nilai > 80 "graduate"
      nilai > 90 "honour"
  
      output:
      1. Email: abduh@mail.com
      Rata-rata: 85.7
      Predikat: graduate
  
      2. Email: khairun@mail.com
      Rata-rata: 89.3
      Predikat: graduate
  
      3. Email: bondra@mail.com
      Rata-rata: 74.3
      Predikat: participant
  
      4. Email: regi@mail.com
      Rata-rata: 91
      Predikat: honour
  
  */

  
// Jawaban Soal 3 Recap Score
 
function recapScores(data) {
    const [_, ...datas] = data;

    const sum       = x => x.reduce((a,b) => a + b, 0);
    const avg       = x => sum(x) / x.length;
    const predicate = x => avg(x) > 90 ? 'honour' : avg(x) > 80 ? 'graduate' : avg(x) > 70 ? 'participant' : null; // no condition given for <= 70
    const round     = x => Math.round(x * 10) / 10;

    const objects = datas.map(([email, ...scores]) => ({email, avg: avg(scores), predicate: predicate(scores) }));

    // assuming output is console log
    objects.forEach((o,i) => {
      console.log(`${i + 1}. Email: ${o.email}\nRata-rata: ${round(o.avg)}\nPredikat: ${o.predicate}\n`);
    })

    // assuming output is array of objects
    return objects
  }
  
recapScores(data);
