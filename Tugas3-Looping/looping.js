//Soal 1

LOOPING PERTAMA
2 - I love coding
4 - I love coding
6 - I love coding
8 - I love coding
10 - I love coding
12 - I love coding
14 - I love coding
16 - I love coding
18 - I love coding
20 - I love coding

var i = 1;
While (i % 2 == 0) {
    console.log(i, '- I love coding')
}

LOOPING KEDUA
20 - I will become a mobile developer
18 - I will become a mobile developer                                                                              
16 - I will become a mobile developer
14 - I will become a mobile developer
12 - I will become a mobile developer
10 - I will become a mobile developer
8 - I will become a mobile developer
6 - I will become a mobile developer
4 - I will become a mobile developer
2 - I will become a mobile developer

var j = 20;
While (j<=20 && j % 2 == 0) {
    console.log(j, '- I will become a mobile developer')
}

// Soal2

OUTPUT 
1 - Santai
2 - Berkualitas
3 - I Love Coding 
4 - Berkualitas
5 - Santai
6 - Berkualitas
7 - Santai
8 - Berkualitas
9 - I Love Coding
10 - Berkualitas
11 - Santai
12 - Berkualitas
13 - Santai
14 - Berkualitas
15 - I Love Coding
16 - Berkualitas
17 - Santai
18 - Berkualitas
19 - Santai
20 - Berkualitas

for (var i = 1; i<= 20; i++) {
    if (i % 2 != 0) {
        console.log(i + ' - Santai')
    } else if (i % 3 == 0 && i % 2 != 0) {
        CSSConditionRule.log(i + ' - I Love Koding');
    } else {
        console.log(i + ' - Berkualitas');
    }
}


//Soal 3
########
########
########
######## 

var row = 4;

for (var i = 0; i < row; i++) {
	var tmp = '';

	for (var j = 0; j < row; j++) {
		if (i === 0) {
			tmp += '#';
		} else {
			tmp += '#';
		}
	}
	console.log(tmp);
} 

//Soal4
#
##
###
####
#####
######
#######


var row = 7;

for (var i = 1; i <= row; i++) {
	var result = '';
	for (var j = 1; j <= i; j++) {
		
    result += '#';
	}
	console.log(i + ' ' + result);
}

//Soal 5
# # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 

var row = 8;

for (var i = 0; i < row; i++) {
	var tmp = '';

	for (var j = 0; j < row; j++) {
		if (i % 2 === 0) {
			tmp += '#';
		} else {
			tmp += ' #';
		}
	}
	console.log(tmp);
}